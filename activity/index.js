
// No. 4

let trainer = { 
	name: "Brandon Lee Romero",
	age: 22,
	pokemon: ['Rayquaza', 'Raikou', 'Entei', 'Suicune'],

	friends: {
		stoThomas:  ['Figeuroa', 'Bulaon'],
		staMonica:  ['Alessandra', 'Denise']
		
	},

// No. 5

	talk: function(){
		return `${this.pokemon[0]}! I choose you!`
	}

}

// No. 6
console.log(trainer)
console.log('Result of dot notation:')
console.log(trainer.name)
console.log('Result of bracket notation:')
console.log(trainer['pokemon'])

// No.7
console.log('Result of talk method:')
console.log(trainer.talk())

// No. 8
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 1.25 * level;
	this.attack = 2 * level;

// No. 9
	this.tackle = function(target) {
		target.health = target.health - this.attack

		console.log(this.name + " tackled " + target.name)

		console.log(target.name + "'s health is now reduced to " + target.health)

		if (target.health <= 0){

			target.faint(target.name)
		}
	}

	this.faint = function(name){
		console.log(this.name + ' fainted.')
	}
}

let rayquaza = new Pokemon('Rayquaza', 75)
let entei = new Pokemon('Entei', 80)
let suicune = new Pokemon('Suicune', 85)
let raikou = new Pokemon('Raikou', 90)

console.log(rayquaza)
console.log(entei)
console.log(suicune)
console.log(raikou)

rayquaza.tackle(entei)

console.log(Pokemon.tackle)
console.log(rayquaza)
console.log(entei)
